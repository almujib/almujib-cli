const settings = require("./package");

const path = require("path");

process.env.VERSION = settings.version;
process.env.HOME_PAGE = settings.homepage;
process.env.WIKI = settings.wiki;
process.env.ROOT = path.resolve(__dirname);

process.env.PATH_DATA = path.join("data");
process.env.PATH_DOCS = path.join("docs");

process.env.DEPENDENCIES = Object.keys(settings.dependencies);