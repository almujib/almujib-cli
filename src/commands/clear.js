const clear = {
	command: ["clear", "cls"],
	describe: "Show information of date now",
	handler() {
		console.clear();
	}
}

module.exports = clear;