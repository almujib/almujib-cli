const set = yargs => {
	yargs.command(require("./version"));
	yargs.command(require("./echo"));
	yargs.command(require("./kbbi"));
	yargs.command(require("./timer"));
	yargs.command(require("./device"));
	yargs.command(require("./date"));
	yargs.command(require("./clear"));
	yargs.command(require("./random"));
	yargs.command(require("./repo"));
	yargs.command(require("./vim"));
	yargs.command(require("./uid"));
	yargs.command(require("./dependencies"));
	
	require("./web-apps/init")(yargs);
}

module.exports = set;