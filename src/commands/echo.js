const echo = {
	command: "echo [string]",
	describe: "Echo [string] on console",
	builder: {
		string: {
			demandOption: false,
			type: "string"
		}
	},
	handler(argv) {
		console.log(argv.string);	
	}
}

module.exports = echo;