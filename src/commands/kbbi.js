const KBBI = require("kbbi.js");

const kbbi = {
	command: ["kbbi [query]", "KBBI [query]"],
	describe: "KBBI (Kamus Besar Bahasa Indonesia",
	builder: {
		query: {
			describe: "Query for search (KBBI)",
			demandOption: true,
			type: "string"
		}
	},
	handler(argv) {
		KBBI.cari(argv.query)
			.then((e) => {
				console.log("KBBI (Kamus Besar Bahasa Indonesia)");
				console.log(`QUERY: ${argv.query}`);
				console.log(`LEMA: ${e.lema}`);
				for (const data of e.arti) console.log(`=> ${data}`);
			})
			.catch(e => console.log(`ERROR: ${e}`));
	}
}

module.exports = kbbi;