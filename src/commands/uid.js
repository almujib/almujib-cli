const uid_module = require("uid");

const uid = {
	command: "uid [length]",
	describe: "Print id [length] on console",
	builder: {
		length: {
			demandOption: false,
			type: "number"
		}
	},
	handler(argv) {
		const result = uid_module.uid(argv.length ? argv.length : 11);
		console.log(result);	
	}
}

module.exports = uid;