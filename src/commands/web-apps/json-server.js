const fs = require("fs");
const app = require("express")();

const server = {
	command: ["json-server [json_file] [port]", "server-json [json_file] [port]"],
	describe: "Create server json viewer",
	builder: {
		json_file: {
			demandOption: true,
			type: "string"
		},
		port: {
			demandOption: false,
			type: "number"
		}
	}, 
	handler(argv) {
		const port = argv.port || 3000;

		app.all("*", (req, res) => {
			const context = JSON.parse(fs.readFileSync(argv.json_file).toString());
			res.send(context);
		});
		app.listen(port, () =>console.log(`Listening on http://localhost:${port}`));
	}
}

module.exports = server;