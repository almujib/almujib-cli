const app = require("express")();

const server = {
	command: ["server-test [port]", "server-testing [port]"],
	describe: "Create server testing",
	builder: {
		port: {
			demandOption: false,
			type: "number"
		}
	}, 
	handler(argv) {
		const port = argv.port || 3000;

		app.all("*", (req, res) => {
			const context = {
				url: {
					method: req.method,
					protocol: req.protocol,
					host: req.hostname,
					path: req.path,
					original_url: req.originalUrl
				},
				query: req.query,
				message: "SERVER TESTING OK!",
				status: true
			}

			res.send(context);
		});
		app.listen(port, () =>console.log(`Listening on http://localhost:${port}`));
	}
}

module.exports = server;