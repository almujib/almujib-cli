if (!navigator.onLine) swal("Warning", "Internet Not Found!", "warning");

document.getElementById("btn-back").addEventListener("click", () => history.back());
document.getElementById("btn-forward").addEventListener("click", () => history.forward());

const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));