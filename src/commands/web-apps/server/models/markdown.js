const fs = require("fs");

const MarkdownIt = require("markdown-it");
const md = new MarkdownIt();

const fetch = file_name => {
	const data = fs.readFileSync(file_name).toString();
	return md.render(data);
}

module.exports = fetch;