const express = require("express");
const app = express();

const server = {
	command: "server [port]",
	describe: "Create server starter",
	builder: {
		port: {
			demandOption: false,
			type: "number"
		}
	}, 
	handler(argv) {
		const port = argv.port || 3000;
		app.set("view engine", "ejs");
		app.set("views", __dirname + "/views");
		app.engine("ejs", require("express-ejs-extend"));
		app.use("/public", express.static(__dirname + "/public"));
		app.use(require("cookie-parser")());

		app.use((req, res, next) => {
			req.context = {}
			req.context.version = process.env.VERSION;
			next();
		});
		// TODO
		// app.use(require("./middlewares/security"));

		// ROUTES
		app.get("/", (req, res) => res.redirect("/home"));
		app.use("/home", require("./routes/home"));
		app.use("/account", require("./routes/account"));
		app.use("/calendar", require("./routes/calendar"));
		app.use("/docs", require("./routes/docs"));
		
		app.all("*", (req, res) => res.redirect("/home/404"));

		app.listen(port, () =>console.log(`Listening on http://localhost:${port}`));
	}
}

module.exports = server;