// All pages except:
// home/index
// home/about
// home/lisence
// home/apps
// account/signin
// account/signup (except this if cookie account not founded)
const security = (req, res, next) => {
	const excepted = [
		"/", 
		"/favicon.ico", 
		"/home", "/home/index", "/home/about", "/home/404", "/home/license", "/home/apps",
		"/account/signin", "/account/signup"
		];
	if (!excepted.includes(req.url)) {
		res.redirect("/account/signup");
	}
	next();
}

module.exports = security;