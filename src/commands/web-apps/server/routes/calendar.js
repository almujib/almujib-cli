const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
	const context = req.context;
	res.render("pages/calendar/index", context);
});

router.get("/masehi", (req, res) => {
	const context = req.context;
	res.render("pages/calendar/masehi", context);
});

router.get("/hijriah", (req, res) => {
	const context = req.context;
	res.render("pages/calendar/hijriah", context);
});

module.exports = router;