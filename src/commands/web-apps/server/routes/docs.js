const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
	const context = req.context;
	res.render("pages/docs/index", context);
});

module.exports = router;