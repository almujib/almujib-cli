const express = require("express");
const router = express.Router();

const markdown = require("../models/markdown");

router.get("/404", (req, res) => {
	const context = req.context;
	context.title_page = "404";
	res.render("pages/home/404", context);
});

router.get("/", (req, res) => {
	const context = req.context;
	res.render("pages/home/index", context);
});

router.get("/license", (req, res) => {
	const context = req.context;
	context.title_page = "Lisence";
	context.content = markdown(process.env.ROOT + "/LICENSE");
	res.render("pages/home/license", context);
});

router.get("/about", (req, res) => {
	const context = req.context;
	context.title_page = "About";
	context.content = markdown(process.env.ROOT + "/README.md");
	res.render("pages/home/about", context);
});

router.get("/settings", (req, res) => {
	const context = req.context;
	context.title_page = "Settings";
	context.data = {};
	context.data.cookies = req.cookies;
	res.render("pages/home/settings", context);
});

router.get("/apps", (req, res) => {
	const context = req.context;
	context.title_page = "Apps";
	context.apps = require(process.env.ROOT + "/apps.json").shuffle();
	res.render("pages/home/apps", context);
});

module.exports = router;