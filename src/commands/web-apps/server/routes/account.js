const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
	const context = req.context;
	context.title_page = "Account";
	res.render("pages/account/index", context);
});

router.get("/signin", (req, res) => {
	const context = req.context;
	context.title_page = "Sign In";
	res.render("pages/account/signin", context);
});

router.get("/signup", (req, res) => {
	const context = req.context;
	context.title_page = "Sign Up";
	res.render("pages/account/signup", context);
});

module.exports = router;