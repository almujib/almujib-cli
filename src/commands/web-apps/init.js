const set = yargs => {
	yargs.command(require("./json-server"));
	yargs.command(require("./server-test"));
	
	yargs.command(require("./server/index"));
}

module.exports = set;