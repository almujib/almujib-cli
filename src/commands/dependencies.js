const dependencies = {
	command: ["dependencies", "packages"],
	describe: "Show packages",
	handler() {
		let data = process.env.DEPENDENCIES.split(",");
		console.log("DEPENDENCIES: ");
		console.table(data.shuffle());
	}
}

module.exports = dependencies;