const vim = {
	command: "vim [path]",
	describe: "Open vim - Vi IMproved, a programmer's text editor",
	handler(argv) {
		console.editor(argv.path);
	}
}

module.exports = vim;