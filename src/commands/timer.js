const timer = {
	command: "timer [second]",
	describe: "Timer",
	builder: {
		second: {
			demandOption: true,
			type: "number"
		}
	},
	handler(argv) {
		let i = 1;
		setInterval(function() {
			console.log(`TIMER: ${i} second of ${argv.second} second`);
			if (i == argv.second) clearInterval(this);
			i++;
		}, 1000);
	}
}

module.exports = timer;