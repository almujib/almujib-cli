const date = {
	command: "wiki",
	describe: "Show link of wiki",
	handler() {
		console.log(`Wiki: ${process.env.WIKI}`);
	}
}

module.exports = date;