const date = {
	command: ["date", "time"],
	describe: "Show information of date now",
	handler() {
		console.log(`Date: ${new Date()}`);
	}
}

module.exports = date;