const os = require("os");

const device = {
	command: ["device", "device-information"],
	describe: "Show information of device",
	handler() {
		console.log(os.cpus());	
	}
}

module.exports = device;