const randomstring = require("randomstring");

const random = {
	command: "random [type] [length] [min] [max] [charset]",
	describe: "Generate random by type",
	builder: {
		type: {
			describe: "Type result",
			demandOption: true,
			choices: ["string", "int", "float", "bool"],
			type: "string"
		},
		length: {
			describe: "Length of result (only for result string)",
			demandOption: false,
			type: "number"
		},
		min: {
			describe: "Min range (only for result int or float)",
			demandOption: false,
			type: "number"
		},
		max: {
			describe: "Max range (only for result int or float)",
			demandOption: false,
			type: "number"
		},
		charset: {
			describe: "Charset range (only for result string)",
			demandOption: false,
			type: "string"
		}
	},
	handler(argv) {
		if (argv.type == "string") {
			console.log(randomstring.generate({
				length: argv.length || 10,
				charset: argv.charset || "alphabetic"
			}));
		} else if (argv.type == "bool") {
			console.log(Math.random() < 0.9); //90% probability of getting true
		} else if (argv.type == "int") {
			const max = argv.max || 10;
			const min = argv.min || 0;
			console.log(Math.floor(Math.random() * max) + min);
		} else if (argv.type == "float") {
			const max = argv.max || 10;
			const min = argv.min || 0.102;
			console.log(Math.random() * (max - min) + min);
		}
	}
}

module.exports = random;
