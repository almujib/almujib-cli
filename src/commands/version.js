const version = {
	command: ["version", "v"],
	describe: "Show version",
	handler() {
		console.log(process.env.VERSION);
	}
}

module.exports = version;