const yargs = require("yargs");

require("./commands/init")(yargs);
require("../env");

yargs.parse();