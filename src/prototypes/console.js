const child_process = require("child_process");

const clear = () => process.stdout.write("\033c");

const cls = () => process.stdout.write("\033c");

const editor = (path="file.txt") => { 
	const editor = process.env.EDITOR || "nano";
	const child = child_process.spawn(editor, [path], {
	    stdio: "inherit"
	});

	child.on("exit", function (e, code) {
	    console.log("FINISHED");
	});
}

console.clear = clear;
console.cls = cls;
console.editor = editor;